package no.uib.inf101.terminal;

// UiB INF101 ShellLab - SimpleShell.java
//
// Dette er klassen vi skal forbedre i denne lab'en. Slik koden er
// allerede før du begynner på laben, støtter den tre kommandoer:
//
// - cd: Bytt til en annen mappe
// - ls: List opp filer i mappen
// - pwd: Vis sti til nåværende mappe
//
// Vi skal endre denne klassen slik at den
// - kan vises av Terminal.java
// - kan støtte ubegrenset mange kommandoer

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.HashMap;

// TODO 1: Let SimpleShell implement CommandLineInterface
public class SimpleShell implements CommandLineInterface  {

  private final HashMap<String, Command> allCommands = new HashMap<>();
  /** The prompt to show before each command */
  private final String prompt = "$ ";
  /** The context variable contains cwd and home directories etc. */
  private final Context context = new Context();
  /** A list of historic commands and their outputs */
  private final List<String> history = new ArrayList<>();
  /** The command currently being typed */
  private String currentCommand = "";
  // TODO 4: Create instance variable storing Command objects

  //////////////////////////////////////////////////////////////////////
  /// Public instance methods                                     //////
  /// (methods expected to be used by someone outside this class) //////
  //////////////////////////////////////////////////////////////////////

  /** Constructor for SimpleShell */
  public SimpleShell() {
    installCommands(); 
  }

  private void installCommands() {
    this.installCommand(new CmdLs());
    this.installCommand(new CmdPwd());
    this.installCommand(new CmdCd());
    // Legg til flere kommandoer etter behov
  }

  public class HelloWorld {
    public static void main(String[] args) {
      System.out.println("Hello World!");
    }
  }




  @Override
  public void keyPressed(char key) {
    System.out.println("Keycode pressed: '" + ((int) key) + "'");

    if (key == '\n') {
        processCurrentCommandLine();
    } else if (key == '\b') { // Sjekk om backspace-tasten er trykket
        if (currentCommand.length() > 0) {
            // Fjern bakerste bokstav hvis kommandoen ikke er tom
            currentCommand = currentCommand.substring(0, currentCommand.length() - 1);
        }
    } else if (key >= ' ' && key <= '~') {
        currentCommand += key;
    } else {
        // Noe spesiell tast ble trykket (f.eks. shift, ctrl), ignorér det
    }
  }

  @Override
  public String getScreenContent() {
    String s = "";
    for (String line : this.history) {
      s += line;
    }
    s += this.prompt;
    s += this.currentCommand;
    return s;
  }

  public void installCommand(Command command) {
    command.setContext(this.context);
    String commandName = command.getName();
    // Add the command to the allCommands map
    this.allCommands.put(commandName, command);
  }

  /**
   * Process the current command line. This entails splitting it into
   * a command name and arguments; executing the command; and adding
   * the result to the history.
   */
  private void processCurrentCommandLine() {
    String result = "";
    if (this.currentCommand.length() > 0) {
      String[] args = this.currentCommand.split(" ");
      String commandName = args[0];
      String[] commandArgs = new String[args.length - 1];
      System.arraycopy(args, 1, commandArgs, 0, commandArgs.length);
      result = this.executeCommand(commandName, commandArgs);
      if (result.length() > 0 && result.charAt(result.length() - 1) != '\n') {
        result += '\n';
      }
    }
    this.history.add(this.prompt + this.currentCommand + "\n" + result);
    this.currentCommand = "";
  }

  /**
   * Execute a command with the given name and arguments. The command
   * name could be "ls", "cd", "pwd", etc., and the arguments are the
   * arguments to the command. For example for the command "cd foo", the
   * command name is "cd" and the argument comes in the array ["foo"].
   *
   * @param commandName  The name of the command to execute
   * @param args  The arguments to the command
   * @return  The output of the command
   */
  private String executeCommand(String commandName, String[] args) {
    // TODO 6: Call run on Command object for given commandName if present
    // TODO 7-8-9: Remove if's for cd, ls, pwd once installed as commands
    Command command = this.allCommands.get(commandName);
    if (command != null) {
      String result = command.run(args);
      return result;
    } else {
      return "Command not found: \"" + commandName + "\"";
    }

  }

  public class CmdLs implements Command {
    private Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        StringBuilder result = new StringBuilder();
        File[] files = context.getCwd().listFiles(); // Henter en liste over filer i nåværende arbeidsmappe

        if (files != null) {
            for (File file : files) {
                result.append(file.getName()).append(" "); // Legger til filnavnet til resultatet, med et mellomrom etter hvert navn
            }
        } else {
            result.append("No files in directory.");
        }

        return result.toString();
    }
    @Override
    public String getName() {
        return "ls"; // Returnerer navnet på kommandoen
    }
  }

  public class CmdPwd implements Command {
    private Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        return context.getCwd().getAbsolutePath();
    }
    @Override
    public String getName() {
        return "pwd"; // Returnerer navnet på kommandoen
    }
  }


  public class CmdCd implements Command {
    private Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        if (args.length == 0) {
            context.goToHome();
            return "";
        } else if (args.length > 1) {
            return "cd: too many arguments";
        }
        String path = args[0];
        if (context.goToPath(path)) {
            return "";
        } else {
            return "cd: no such file or directory: " + path;
        }
    }
    @Override
    public String getName() {
        return "cd"; // Returnerer navnet på kommandoen
    }
  }




  @Override
  public void installCommand(CmdJavaProgram cmdJavaProgram) {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("Unimplemented method 'installCommand'");
  }

}
